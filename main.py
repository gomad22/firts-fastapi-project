
from fastapi import FastAPI #Body = Pedir input como Json, Path = Especificaciones para parámetros, Query = Especificaciones para parámetros query
 #Base Model para crear a clase y Field para validación de datos específicos
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router

#Creacion de la app
app = FastAPI()

#Cambio de Titulo de la app
app.title = "Wohooo mis propios EP's"
#Cambio de versión
app.version = "0.0.1"

#Adicionar middleware
app.add_middleware(ErrorHandler)

#Adicionar las rutas
app.include_router(movie_router)
app.include_router(user_router)

#Creación de la base de datos
Base.metadata.create_all(bind=engine)




