from models.movie import Movie as MovieModel
from schemas.movie import Movie

#Creación de la clase
class MovieService():

    #Metodo de inicialización
    def __init__(self, db) -> None:
        self.d = db
    
    #Metodo de query para consultar todo
    def get_movies(self):
        result = self.db.query(MovieModel).all()
        return result 

    #Metodo de query para consultar por id
    def get_movie(self, id):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        return result 

    #Metodo de query para consultar por categoria
    def get_movie_by_category(self, category):
        result = self.db.query(MovieModel).filter(MovieModel.category == category).all()
        return result 

    #Metodo para crear una pelicula
    def create_movie(self, movie: Movie):
        new_movie = MovieModel(**movie.dict())
        self.db.add(new_movie)
        self.db.commit()
        return

    #Metodo para actualizar una pelicula
    def update_movie(self, id: int, data: Movie):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        result.title = data.title
        result.overview = data.overview
        result.year = data.year
        result.rating = data.rating
        result.category = data.category
        self.db.commit()
        return

    #Metodo para eliminar una pelicula
    def delete_movie(self, movie:Movie):
        self.db.delete(movie)
        self.db.commit()
        return