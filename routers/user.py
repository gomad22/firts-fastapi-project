from fastapi import APIRouter, Request, Depends #Body = Pedir input como Json, Path = Especificaciones para parámetros, Query = Especificaciones para parámetros query
from fastapi.responses import HTMLResponse, JSONResponse
from utils.jwt_manager import create_token, validate_token
from fastapi.security import HTTPBearer
from config.database import Session
from models.movie import Movie as MovieModel
from schemas.user import User

user_router = APIRouter()

#Clase de para validar el token
class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data['email'] != 'admin@gmail.com':
            raise HTMLResponse(status_code=403, detail='Credenciales invalidas')

# Logins del usuario
@user_router.post('/login', tags=['auth'])
def login(user: User):
    if user.email == "admin@gmail.com" and user.password == "admin":
        token: str = create_token(user.dict())
        return JSONResponse(status_code=200, content=token)
    

#Un metodo con dependencia de login
@user_router.get('/movies_usuario', tags=['Movies con usuario'], dependencies=[Depends(JWTBearer())])
def get_movies():
    db = Session()
    #Se extraen los resultados de las bases de datos
    result = db.query(MovieModel).all()
    return result