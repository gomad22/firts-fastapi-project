from fastapi import APIRouter
from fastapi import Path, Query #Body = Pedir input como Json, Path = Especificaciones para parámetros, Query = Especificaciones para parámetros query
from fastapi.responses import JSONResponse
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()


#Metodo que retorna las peliculas dentro de la base de datos
@movie_router.get('/movies', tags=['Movies'])
def get_movies():
    #Se abre de nuevo sesión en la base de datos
    db = Session()
    #Se extraen los resultados de las bases de datos
    result = MovieService(db).get_movies()
    return result

#Metodos para recibir parámetros en la ruta (En este cso el ID)
#Ahora con base de datos
@movie_router.get('/movies/{id}', tags =['Movies'])
def get_movie_by_id(id: int = Path(ge=0, le=100)): 
    #Se inicializa la sesión de la DB
    db = Session()
    #Se realiza el query
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"Message": "La Pelicula con ese Id no se encontro"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies_by_category/', tags =['Movies'])
def get_movie_by_category(category: str = Query(max_length=100)): 
    #Se inicializa la sesión de la DB
    db = Session()
    #Se realiza el query
    result = MovieService(db).get_movie_by_category(category)
    if not result:
        return JSONResponse(status_code=404, content={"Message": "Peliculas con ese desarrollo no se encontraron"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


#Movies con base de datos
@movie_router.post('/movies/register_movie', tags=['Movies'])
def add_movie_base_model(movie: Movie):
    #Se crea la sesion de la base de datos
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(content={"Message": "Se ha agregado la pelicula a base de datos"})

#Metodos put
#Metodo put con DB
@movie_router.put('/movies_update', tags={'Movies'})
def update_movie(id: int, movie: Movie)-> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
            return JSONResponse(status_code=404, content={'Message': "No se encontro una pelicula con ese id"})
    MovieService(db).update_movie(id, movie)
    return JSONResponse(status_code=200, content={"Message":"La pelicula se ha modificado"})


#Metodos Delete con base de datos
@movie_router.delete('/movies/delete', tags=['Movies'])
def delete_movie(id:int):
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'Message': "No se encontro una pelicula con ese id"})
    MovieService(db).delete_movie(result)
    return JSONResponse(status_code=200, content={"Message":"La pelicula se ha eliminado"})
