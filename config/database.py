import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

#Conexión a la base de datos -> ../ Para retroceder una carpeta
sqlite_file_name = "../databse.sqlite"
base_dir = os.path.dirname(os.path.realpath(__file__))
database_url = f'sqlite:///{os.path.join(base_dir, sqlite_file_name)}'

#Engine de SQLAlchemy
engine = create_engine(database_url, echo=True)

#Sesión para conectarse a db
Session = sessionmaker(bind=engine)

#Instancia de base de datos
Base = declarative_base()