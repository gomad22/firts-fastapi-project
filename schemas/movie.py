from pydantic import BaseModel, Field #Base Model para crear a clase y Field para validación de datos específicos
from typing import Optional

#Creación de la clase que se llama Movie
class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(max_length=20)
    overview: str = Field(default="Muy mala")
    year: Optional[int] = None
    rating: Optional[float] = None
    category: Optional[str] = None

    class Config:
        schema_extra = {
            "exmple": {
                "id": 1,
                "title": "Stuart Little",
                "Overview": "Basica",
                "year": 2001,
                "rating": 7.1,
                "category": "Familia"
            }
        }